import random
import pyjokes
import time
import art


class Film:
    ACTION = ("Темний лицар", "Володар перснів: Хранителі персня", "Початок", "Матриця", "Месники: Завершення")
    COMEDY = ("1+1", "Залягти на дно в Брюге", "Готель «Ґранд Будапешт»", "Три білборди під Еббінґом, Міссурі",
              "Великий Лебовські")
    DRAMA = ("Втеча з Шоушенка", "Хрещений батько", "Список Шиндлера", "Форрест Ґамп", "Пролітаючи над гніздом зозулі",
             "Зелена миля")
    FANTASY = ("Зоряні війни", "Пірати Карибського моря: Прокляття Чорної перлини",
               "Володар перснів: Повернення короля", "Володар перснів: Хранителі персня", "Володар перснів: Дві вежі")
    HISTORY = ("Хоробре серце", "Гра на пониження", "З міркувань совісті", "12 років рабства", "Дюнкерк")
    HORROR = ("Чужий", "Сяйво", "Щось", "Той, що виганяє диявола", "Пила: Ігри на виживання")
    ROMANCE = ("Життя прекрасне", "Твоє ім'я", "Ла Ла Ленд", "Вона", "Титанік")
    THRILLER = ("Сім", "Мовчання ягнят", "Паразити", "Престиж", "Мементо")
    ANIMATION = ("Віднесені привидами", "Король Лев", "Шрек", "Історія іграшок", "У пошуках Немо")


class Music:
    ROCK = ("The Beatles", "Aerosmith", "Metallica", "АС/DC", "Led Zeppelin")
    RAP = ("Jay-Z", "Kendrick Lamar", "Drake", "Eminem", "Snoop dogg")
    JAZZ = ("Луи Армстронг", "Билли Холидей", "Элла Фицджеральд", "Рэй Чарльз", "Диззи Гиллеспи")
    METAL = ("System of a Down", "Disturbed", "Soil", "Slipknot", "Black Sabbath")
    DUBSTEP = ("Skrillex", "Burial", "Nero", "Knife Party", "Pegboard Nerds")
    PHONK = ("Kordhell", "SHADXWBXRN", "MC ORSEN", "Freddie Dredd", "ARCHEZ")


class Videogame:
    SHOOTER = ("Metro Exodus", "BioShock Infinite", "DOOM", "Battlefield 4", "Overwatch", "Borderlands 2",
               "Call of Duty: Modern Warfare", "Sniper Elite", "Gears of War 4", "PLAYERUNKNOWN’S BATTLEGROUNDS")
    RACES = ("Forza Horizon 4", "DiRT Rally", "Gran Turismo 6", "Project CARS", "The Crew", "WRC 5",
             "Need for speed most wanted", "Need for Speed Underground")
    FIGHTING = ("Mortal Kombat X", "Injustice 2", "EA Sports UFC 3", "Street Fighter V", "Tekken 7", "For Honor")
    STRATEGY = ("Divinity: Original Sin 2", "Warcraft 3", "Frostpunk", "Total War: Warhammer",
                "Sid Meier’s Civilization 6", "XCOM 2", "Anno 1800", "Wasteland 3", "Козаки 3",
                "Might & Magic Heroes 3")
    SOULSLIKE = ("Sekiro", "Elden ring", "Dark Souls", "Dark Souls 2", "Dark Souls 3", "Nioh 2", "Demon souls",
                 "Bloodborne", "The Surge 2", "Mortal Shell")
    RPG = ("The Witcher 3: Wild Hunt", "The Legend of Zelda: Breath of the Wild", "The Elder Scrolls V: Skyrim",
           "Cyberpunk 2077", "Diablo III", "Assassin’s Creed", "Fallout 4", "World of Warcraft",
           "South Park: The Fractured but Whole", "Middle-Earth: Shadow of War")


class Joke:
    @staticmethod
    def jokes_en():
        print("__________________________________________________________________________________________________")
        joke_en = pyjokes.get_jokes(language="en", category="all")
        print(joke_en[random.randint(0, len(joke_en))])
        print("__________________________________________________________________________________________________")

    @staticmethod
    def jokes_ua():
        joke_ua = ("Раніше бабусі просили онуків просилити нитку в голку, а в майбутньому проситимуть увести капчу.",
                   "Зупиняє даїшник машину, з машини вивалюється сильно п'яний водій. Даїшник питає:\n— Ваші права?!"
                   "\nВодій відповідає (насилу плентає язиком):\n— Адміністратор!",
                   "Для догляду за літнім програмістом потрібна приємна жінка, що говорить на JАVА, LІSР і С++.",
                   "Учитель у школі запитує, ким у кого з дітей працюють батьки:\n— Марійко, ось у тебе тато ким "
                   "працює?\n— Кухарем!\n— Добре, Марійко! А у тебе, Вовочко, ким?\n— Програмістом..."
                   "\n— Не смійтесь, діти, у хлопчика горе.", "Переїхав програміст із України в Північну Америку."
                   "Надсилає своїм родичам звідти посилку. Родичі здивувались – він зазвичай тільки на Новий Рік "
                   "посилки шле, а тут до Нового Року ще 2 місяці, та й посилка ще в такій коробці, що може слона "
                   "вмістити. Відкривають коробку, вона виявляється всередині пуста, тільки на самому дні якийсь "
                   "папірець лежить. Дістають вони її й бачать, що на тому папірці написано: 'Теst'.",
                   "Син програміста — батькові:\n— Тату, звідки беруться діти?\n— Всередині мами міститься "
                   "компілятор дитини, а всередині батька — її вихідний код: батько передає матері вихідний код дитини,"
                   " починається процес компіляції й наступні 9 місяців компілюється сама дитина.",
                   "Немає неприступних жінок... Є різні рівні доступу!", "Програміст повертається з роботи. Його "
                   "зустрічає дружина із задумливим виглядом:\n— Що так довго?\n— Та як завжди...\n— А у нас уже через "
                   "тиждень дев’ять місяців!\n— Та невже? І хто ж, хлопчик чи дівчинка?\n— Це поки невідомо. "
                   "Лікар сказав, що дослідження шкідливе... Що мовчиш? Ти мене не любиш?! Я завжди це знала! Паскуда!"
                   "\n— Та ні, я просто думаю, якими методами звертатися до об’єкта, чий клас не визначений під "
                   "час розробки...", "Телефонує програміст:\n— Доброго дня, Катю можна?\n— Вона в архіві.\n"
                   "— Розархівуйте її, будь ласка. Вона мені терміново потрібна.", "Програміста запитують:\n"
                   "— Як ви спромоглися так швидко вивчити англійську мову?!\n— Та то такий дріб’язок. Вони там майже "
                   "всі слова з Python узяли.")
        print("__________________________________________________________________________________________________")
        print(joke_ua[random.randint(0, len(joke_ua))])
        print("__________________________________________________________________________________________________")


class Story:
    STORIES = ("Першим запрограмованим пристроєм прийнято вважати жакардовий ткацький верстат, побудований в 1804 році "
               "Жозефом Марі Жаккар. Він здійснив революцію в ткацькій промисловості, надавши можливість програмувати "
               "візерунки на тканинах за допомогою перфокарт.", "Першу в історії людства програму для обчислювальної "
               "машини написала в 1843 році графиня Ада Августа Лавлейс, дочка великого англійського поета Джорджа "
               "Байрона. Ця програма вирішувала рівняння Бернуллі, що виражає закон збереження енергії рухомої рідини. "
               "На честь першої програмістки названа універсальна мова програмування «Ада».", " В 1946 року в Америці "
               "був запущений перший у світі реально програмований електронний комп'ютер ENIAC. ENIAC важив 30 тонн і "
               "складався з 18 тисяч електронних ламп. ENIAC у порівнянні з сучасним ПК був просто черепахою - його "
               "швидкодія було всього 5000 операцій в секунду. Комп'ютер пропрацював дев'ять років до 1955 року. До "
               "нього в світі існували і більш ранні моделі комп'ютерів, але всі вони були експериментальними "
               "варіантами не отримали практичного використання.", "День програміста святкується в 256 день року ("
               "13 вересня, у високосний рік – 12 вересня). Вибір числа 256 не випадковий – воно виходить від виведення"
               " двійки у восьму ступінь. Саме таку кількість чисел можна виразити за допомогою одного восьмирозрядного"
               " байта.", "Перший в континентальній Європі комп’ютер було створено в Україні в 1951 году. Перша ЕОМ "
               "називалась Малою електронною обчислюванною машиною. Вона  нараховувала 6000 електронних ламп та ледь "
               "вмістилась в лівому крилі гуртожитку в селещі Феофанія, що в 10 км від Києва. Машина була побудована в"
               " лабораторії обчислюваної техніки Інституту електротехніки АН УРСР під керівництвом академіка "
               "С.О.Лебедєва.", "Пророцтво майбутнього винаходу iPad зробив академік В.М.Глушков в своїй книзі «Основи"
               " без паперової інформатики»(вийшла друком в 1982) : «Вже недалеко той день, коли зникне звичайна книга,"
               " журнал та газета. Замість цього кожна людина буде носити з собою «електронний» блокнот, що буде "
               "представляти собою комбінацію плоского монітору з мініатюрним радіопередавачем. Набираючи на клавіатурі"
               " цього «блокноту» потрібний код, можна буде (знаходячись в будь-якому місці на нашій планеті) визвати "
               "з гігантських комп’ютерних баз даних, що пов’язані в мережі, будь-які тексти, зображення (в тому числі"
               " і динамічні), які і замінять не тільки сучасні книги, журнали та газети, але і сучасні телевізори.»",
               "«Hello, world!» – програма, результатом роботи якої є виведення на екран або інший пристрій фрази «"
               "Hello, world!» (дослівний переклад з англійської – «Привіт, світ!»). Зазвичай це перший приклад "
               "програми в підручниках з програмування, і для багатьох студентів така програма є першим досвідом при "
               "вивченні нової мови.")


class Game:
    @staticmethod
    def rock_paper_scissors():
        while True:
            player_ = int(input("\n\tЗробіть ваш вибір\n1. Камінь\n2. Ножиці\n3. Папір\n"
                                "0. Повернутися до попереднього меню\n: "))
            time.sleep(1)
            art.tprint("3", font="block", chr_ignore=True)
            time.sleep(1)
            art.tprint("2", font="block", chr_ignore=True)
            time.sleep(1)
            art.tprint("1", font="block", chr_ignore=True)
            time.sleep(1)
            print("__________________________________________________________________________________________________")
            if player_ == 1:
                print("\tВи обрали Камінь")
            elif player_ == 2:
                print("\tВи обрали Ножиці")
            elif player_ == 3:
                print("\tВи обрали Папір")
            elif player_ == 0:
                break
            else:
                print("\tНевірний вибір, спробуйте ще раз\n")
            comp = random.randint(1, 3)
            if comp == 1:
                print("\tКомп'ютер обрав Камінь")
            elif comp == 2:
                print("\tКомп'ютер обрав Ножиці")
            elif comp == 3:
                print("\tКомп'ютер обрав Папір")
            print("__________________________________________________________________________________________________")
            if player_ == comp:
                art.tprint("DRAW", font="block", chr_ignore=True)
            elif player_ == 1 and comp == 2:
                art.tprint("YOU WIN", font="block", chr_ignore=True)
            elif player_ == 1 and comp == 3:
                art.tprint("YOU LOSE", font="block", chr_ignore=True)
            if player_ == 2 and comp == 1:
                art.tprint("YOU LOSE", font="block", chr_ignore=True)
            elif player_ == 2 and comp == 3:
                art.tprint("YOU WIN", font="block", chr_ignore=True)
            if player_ == 3 and comp == 1:
                art.tprint("YOU WIN", font="block", chr_ignore=True)
            elif player_ == 3 and comp == 2:
                art.tprint("YOU LOSE", font="block", chr_ignore=True)
            print("__________________________________________________________________________________________________")

    @staticmethod
    def number_():
        print("\t\n Задайте межі вибору чисел")
        foo = int(input("Найменше число: "))
        bar = int(input("Найбільше число: "))
        comp = random.randint(foo, bar)
        player_ = None
        lifes = 3
        while player_ != comp and lifes != 0:
            player_ = int(input(f"\n\tСпробуйте відгадати випадкове число, у вас залишилося {lifes} спроби: "))
            if comp == player_:
                print("Ви напевно Ванга, ви відгадали :)")
            elif comp > player_:
                print("Невірно, загадане число трохи більше")
                lifes -= 1
            elif comp < player_:
                print("Невірно, загадане число трохи менше")
                lifes -= 1
        print(f"Було загадано число: {comp}")


def recomendation(foo):
    print("__________________________________________________________________________________________________")
    print("\n" + foo[random.randint(0, len(foo) - 1)] + "\n")
    print("__________________________________________________________________________________________________")


while True:
    menu = input("\n\tВиберіть потрібну дію\n1. Порадити цікавий фільм\n2. Порадити музичного виконавця"
                 "\n3. Порадити відеогру\n4. Розповісти анекдот\n5. Розповісти цікавий факт\n6. Зіграти в гру"
                 "\n0. Вийти з програми\n: ")
    match menu:
        case "1":
            while True:
                menu_1 = input("\n\tВиберіть жанр фільма\n1. Екшн\n2. Комедія\n3. Драма\n4. Фентезі\n5. Історичний"
                               "\n6. Фільм жахів\n7. Романтика\n8. Триллер\n9. Мультфільм\n0. Вийти до попереднього "
                               "меню\n: ")
                match menu_1:
                    case "1":
                        recomendation(Film.ACTION)
                    case "2":
                        recomendation(Film.COMEDY)
                    case "3":
                        recomendation(Film.DRAMA)
                    case "4":
                        recomendation(Film.FANTASY)
                    case "5":
                        recomendation(Film.HISTORY)
                    case "6":
                        recomendation(Film.HORROR)
                    case "7":
                        recomendation(Film.ROMANCE)
                    case "8":
                        recomendation(Film.THRILLER)
                    case "9":
                        recomendation(Film.ANIMATION)
                    case "0":
                        break
                    case _:
                        print("Невірно обраний пункт меню")
        case "2":
            while True:
                menu_2 = input("\n\tВиберіть жанр музики\n1. Рок\n2. Реп\n3. Джаз\n4. Метал\n5. Дабстеп\n6. Фонк"
                               "\n0. Вийти до попереднього меню\n: ")
                match menu_2:
                    case "1":
                        recomendation(Music.ROCK)
                    case "2":
                        recomendation(Music.RAP)
                    case "3":
                        recomendation(Music.JAZZ)
                    case "4":
                        recomendation(Music.METAL)
                    case "5":
                        recomendation(Music.DUBSTEP)
                    case "6":
                        recomendation(Music.PHONK)
                    case "0":
                        break
                    case _:
                        print("Невірно обраний пункт меню")
        case "3":
            while True:
                menu_3 = input("\n\tВиберіть жанр гри\n1. Шутер\n2. Гонки\n3. Файтинг\n4. Стратегія\n5. Soulslike\n"
                               "6. Rpg\n0. Вийти до попереднього меню\n: ")
                match menu_3:
                    case "1":
                        recomendation(Videogame.SHOOTER)
                    case "2":
                        recomendation(Videogame.RACES)
                    case "3":
                        recomendation(Videogame.FIGHTING)
                    case "4":
                        recomendation(Videogame.STRATEGY)
                    case "5":
                        recomendation(Videogame.SOULSLIKE)
                    case "6":
                        recomendation(Videogame.RPG)
                    case "0":
                        break
                    case _:
                        print("Невірно обраний пункт меню")
        case "4":
            menu_4_1 = input("\n\tВиберіть мову\n1. Українська\n2. Англійська\n0. Повернутися до попереднього меню\n: ")
            match menu_4_1:
                case "1":
                    Joke.jokes_ua()
                case "2":
                    Joke.jokes_en()
                case 0:
                    break
                case _:
                    print("Невірно обраний пункт меню")
        case "5":
            recomendation(Story.STORIES)
        case "6":
            menu_6 = input("\n\tВиберіть гру\n1. Камінь-ножиці-папір\n2. Вгадай число\n0. Вийти з програми\n: ")
            match menu_6:
                case "1":
                    Game.rock_paper_scissors()
                case "2":
                    Game.number_()
                case "0":
                    break
                case _:
                    print("Невірно обраний пункт меню")
        case "0":
            break
        case _:
            print("Невірно обраний пункт меню")
